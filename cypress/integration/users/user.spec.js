/// <reference types="cypress" />

describe('app', () => {
    context('Load of screen on desktop', () => {
        beforeEach(() => {
            cy.visit('http://localhost:3000')
        })

        it('Show on load', () => {
            cy.get('[data-cy=userMain]').should('be.visible')
            cy.get('[data-cy=userTable]').should('be.visible')
            cy.get('[data-cy=userAccordion').not('be.visible')
        })
    })
})
