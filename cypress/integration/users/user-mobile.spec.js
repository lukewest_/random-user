/// <reference types="cypress" />

const mobileViewport = [
    'iphone-3',
    'iphone-4',
    'iphone-5',
    'iphone-6',
    'iphone-7',
    'iphone-8',
    'iphone-x',
    'iphone-xr',
    'iphone-se2',
    'samsung-note9',
    'samsung-s10',
]

describe('appMobile', () => {
    context('Load of screen on mobile', () => {
        beforeEach(() => {
            cy.visit('http://localhost:3000')
        })

        mobileViewport.forEach((vp) => {
            it(`Should display Main and Accordion on ${vp} screen`, () => {
                if (Cypress._.isArray(vp)) {
                    cy.viewport(vp[0], vp[1])
                } else {
                    cy.viewport(vp)
                }

                cy.get('[data-cy=userMain]').should('be.visible')
                cy.get('[data-cy=userAccordion]').should('be.visible')
            })
        })
    })
})
