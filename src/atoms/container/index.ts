import Container from './container'
import Loading from './loading'
import MainContainer from './main'
import MainTitle from './main-title'
import Section from './section'

export {
    Container,
    Loading,
    MainContainer,
    MainTitle,
    Section
}