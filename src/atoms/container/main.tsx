import styled from 'styled-components'
import { deviceBreakpoint, zindex } from '../../tokens'

const MainContainer = styled.main.attrs({
  'data-cy': 'userMain',
})`
  display: flex;
  flex-direction: column;
  width: calc(100% - 3rem);
  
  position: relative;
  padding: 1.5rem;
  
  ${deviceBreakpoint.desktop.lg} {
    max-width: 1200px;
  }
`

export default MainContainer
