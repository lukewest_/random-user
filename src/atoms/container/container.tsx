import styled from 'styled-components'
import { deviceBreakpoint, zindex } from '../../tokens'

const Container = styled.div`
  display: flex;
  flex-direction: column;
  
  position: relative;
  background-color: var(--ru-white-color);
  min-height: 100vh;

  ${deviceBreakpoint.desktop.lg} {
    align-items: center;
    justify-content: flex-start;
    gap: 1.5rem;
    transition: 0.35s;
  }
`

export default Container
