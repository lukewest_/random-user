import styled from 'styled-components'

interface Props {
    buttonType: string,
    rounded: boolean
}

const ButtonIcon = styled.button<Props>`
  display: block;
  border: 1px solid ${({ buttonType }) =>
    buttonType === 'error' ? 'var(--ru-error-color)' :
        (buttonType === 'info' ? 'var(--ru-info-color)' :
            (buttonType === 'success' ? 'var(--ru-success-color)' :
                (buttonType === 'primary' ? 'var(--ru-primary-color)' : 'transparent')))};
  cursor: pointer;
  border-radius: ${({ rounded }) => (rounded ? '20px' : '2px')};
  color: ${({ buttonType }) => (buttonType !== '' ? 'var(--ru-white-color)' : 'var(--ru-black-color)')};
  background-color: ${({ buttonType }) =>
    buttonType === 'error' ? 'var(--ru-error-color)' :
        (buttonType === 'info' ? 'var(--ru-info-color)' :
            (buttonType === 'success' ? 'var(--ru-success-color)' :
                (buttonType === 'primary' ? 'var(--ru-primary-color)' : 'transparent')))};
  
  margin: 0;
  padding: 0;
  
  width: 30px;
  height: 30px;

  > svg {
    font-size: 16px;
    font-weight: 100;
  }
`

export default ButtonIcon
