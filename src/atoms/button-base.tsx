import React from 'react'
import styled from 'styled-components'

interface Props {
    buttonType: string,
    rounded?: boolean,
    handleClick?: any,
    fullWidth?: string,
    hasIcon?: boolean,
    hideOnMobile?: boolean
}

const ButtonBase = styled.button<Props>`
      display: ${({ hideOnMobile }) => (hideOnMobile ? 'none' : 'block')};
      padding: 10px 25px;
      cursor: pointer;
      border: ${({ buttonType }) =>
        buttonType === 'error' ? 'var(--ru-error-color)' :
            (buttonType === 'info' ? 'var(--ru-info-color)' :
                (buttonType === 'success' ? 'var(--ru-success-color)' :
                    (buttonType === 'primary' ? 'var(--ru-primary-color)' : 'transparent')))};
      background-color: ${({ buttonType }) =>
        buttonType === 'error' ? 'var(--ru-error-color)' :
            (buttonType === 'info' ? 'var(--ru-info-color)' :
                (buttonType === 'success' ? 'var(--ru-success-color)' :
                    (buttonType === 'primary' ? 'var(--ru-primary-color)' : 'transparent')))};
      color: ${({ buttonType }) => (buttonType !== '' ? 'var(--ru-white-color)' : 'var(--ru-black-color)')};
      border-radius: ${({ rounded }) => rounded && '20px'};

      transition: background-color 0.2s ease-out 0s;
      
      font-size: 16px;
      font-weight: bold;
      
      > svg {
        font-size: 16px;
        font-weight: 100;
        padding-left: ${({ hasIcon }) => (hasIcon ? '.5rem' : '0')};
      }
`

export default ButtonBase