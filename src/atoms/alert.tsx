import styled from 'styled-components'
import { deviceBreakpoint } from '../tokens'

interface Props {
    buttonType: string
}

const Alert = styled.div<Props>`
  padding: 1rem 2rem;
  border-radius: 10px;
  border: ${({ buttonType }) =>
          buttonType === 'error' ? 'var(--ru-error-color)' :
                  (buttonType === 'info' ? 'var(--ru-info-color)' :
                          (buttonType === 'success' ? 'var(--ru-success-color)' :
                                  (buttonType === 'primary' ? 'var(--ru-primary-color)' : 'transparent')))};
  background-color: ${({ buttonType }) =>
          buttonType === 'error' ? 'var(--ru-error-color)' :
                  (buttonType === 'info' ? 'var(--ru-info-color)' :
                          (buttonType === 'success' ? 'var(--ru-success-color)' :
                                  (buttonType === 'primary' ? 'var(--ru-primary-color)' : 'transparent')))};
  color: ${({ buttonType }) => (buttonType !== '' ? 'var(--ru-white-color)' : 'var(--ru-black-color)')};
  transition: background-color 0.2s ease-out 0s;

  font-size: 20px;
`

export default Alert