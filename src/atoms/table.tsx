import styled from 'styled-components'
import { deviceBreakpoint } from '../tokens'

const Table = styled.table.attrs({
  'data-cy': 'userTable',
})`
  display: none;
  
  ${deviceBreakpoint.desktop.lg} {
    display: table;
    width: 100%;
    background: var(--ru-white-color);
    box-shadow: 0 0.16rem 0.36rem 0 rgb(0 0 0 / 13%), 0 0.03rem 0.09rem 0 rgb(0 0 0 / 11%);
    border-radius: 10px;
    overflow: hidden;
    transition: color 0.35s ease-in-out;
    border-collapse: collapse;

    > thead {
      background-color: var(--ru-border-tertiary-color);

      tr {
        th {
          font-size: 18px;
          padding: 1rem;
          text-align: left;
          color: var(--ru-border-primary-color);

          &:last-child {
            text-align: center;
          }
        }
      }
    }

    > tbody {
      background-color: var(--ru-white-color);

      tr {
        border-bottom: 1px solid var(--ru-border-tertiary-color);

        td {
          color: var(--ru-heading-color);
          padding: 1rem;
          text-align: left;
          
          &:last-child {
            display: flex;
            justify-content: space-evenly;
          }
        }
        
        &:hover,
        &:focus {
          background-color: var(--ru-primary-color);
          
          td {
            color: var(--ru-white-color);
            
            svg {
              color: var(--ru-white-color);
            }
          }
        }
        
        &:last-child {
          border-bottom: none;
        }
      }
    }
  }
`

export default Table