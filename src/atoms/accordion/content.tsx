import styled from 'styled-components'

const AccordionContent = styled.div`
  color: var(--ru-heading-color);
  padding: 1rem;
`

export default AccordionContent