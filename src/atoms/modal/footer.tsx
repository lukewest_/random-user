import styled from 'styled-components'

interface Props {
    hideAction?: boolean
}

const ModalFooter = styled.div<Props>`
  display: ${({ hideAction }) => hideAction ? 'none': 'flex'};
  flex-wrap: wrap;
  align-items: center;
  justify-content: flex-end;
  gap: 1.5rem;
  
  padding: 1.5rem;
`

export default ModalFooter