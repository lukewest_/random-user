import React, { useEffect } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'
import styled from 'styled-components'
import {
    ButtonBase,
    ButtonIcon,
    ModalBody,
    ModalContainer,
    ModalContent,
    ModalHeader,
    ModalFooter,
    ModalOverlay
} from '../atoms'

const ModalClose = styled.div`
  cursor: pointer;
  pointer-events: all;
  color: var(--ru-border-primary-color);
  
  > svg {
    font-size: 1.25rem;
  }
`

const ModalHeading = styled.h5`
  font-size: 1.25rem;
  color: var(--ru-border-primary-color);
  margin: 0;
  padding: 0;
`

type ModalProps = {
    children?: any,
    heading?: string,
    handleModal?: any,
    handleAction?: any,
    hideAction?: boolean,
    actionTitle: string,
    actionButtonType: string,
    modalWidth: number,
    isGridView?: false
}

const defaultProps: ModalProps = {
    actionButtonType: 'primary',
    actionTitle: 'Save',
    modalWidth: 500
}

const Modal: React.FC<ModalProps> = (props) => {
    return (
        <ModalContainer>
            <ModalBody width={props.modalWidth}>
                <ModalHeader>
                    <ModalHeading>
                        {props.heading}
                    </ModalHeading>
                    <ModalClose onClick={() => props.handleModal(false)}>
                        <FontAwesomeIcon icon={ faTimes } />
                    </ModalClose>
                </ModalHeader>
                <ModalContent>
                    { props.children }
                </ModalContent>
                <ModalFooter hideAction={props.hideAction}>
                    <ButtonBase rounded buttonType="" onClick={() => props.handleModal(false)}>
                        Cancel
                    </ButtonBase>
                    <ButtonBase buttonType={props.actionButtonType} rounded onClick={() => props.handleAction()}>{ props.actionTitle }</ButtonBase>
                </ModalFooter>
            </ModalBody>
            <ModalOverlay />
        </ModalContainer>
    )
}

Modal.defaultProps = defaultProps

export default Modal