import React, { useEffect, useState } from 'react'
import { Route, Routes } from 'react-router-dom'

import {
    MainContainer,
} from '../atoms'
import {
    Users,
    User
} from '../pages'
import {
    UserContext,
    getUsers
} from '../common'

const Main = () => {
    const [users, setUsers] = useState({})
    const [loading, setLoading] = useState(true)

    const loadUsers = async () => {
        const response = await getUsers()
        setLoading(false)
        setUsers(response)
    }

    useEffect(() => {
        loadUsers()
    }, [])

    return (
        <UserContext.Provider
            value={{ users }}
        >
            <MainContainer>
                <Routes>
                    <Route path="/" element={<Users loading={loading} />} />
                    <Route path="/:id" element={<User />} />
                </Routes>
            </MainContainer>
        </UserContext.Provider>
    )
}

export default Main
