import { environment } from './environment.dev'

export const getUsers = async () => {
    const apiResponse = await fetch(`${environment.apiRest}/users`, {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' },
    })
    const data = await apiResponse.json()
    return data
}

export const getUserById = async (userId: any) => {
    const apiResponse = await fetch(`${environment.apiRest}/users/${userId}`, {
        method: 'GET',
        headers: { 'Content-Type': 'application/json'},
    })
    const data = await apiResponse.json()
    return data
}

export const createUser = async (user: any) => {
    const apiResponse = await fetch(`${environment.apiRest}/users`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
            title: user.title,
            first_name: user.firstName,
            last_name: user.lastName,
            email: user.email
        })
    })
    const data = await apiResponse.json()
    return data
}

export const updateUser = async (user: any, userId: string) => {
    const apiResponse = await fetch(`${environment.apiRest}/users/${userId}`, {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
            title: user.title,
            first_name: user.firstName,
            last_name: user.lastName,
            email: user.email
        })
    })
    const data = await apiResponse.json()
    return data
}

export const deleteUser = async (userId: string) => {
    const apiResponse = await fetch(`${environment.apiRest}/users/${userId}`, {
        method: 'DELETE',
        headers: { 'Content-Type': 'application/json' },
    })
    const data = await apiResponse.json()
    return data
}