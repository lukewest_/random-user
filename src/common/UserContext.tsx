import React, { createContext } from 'react'

const defaultState = {
    users: {}
}

export const UserContext = createContext(defaultState)
