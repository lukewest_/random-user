import React from 'react'
import { ThreeDots } from 'react-loader-spinner'
import { Loading } from '../atoms'

const LoadingScreen = ({
   width = 100,
   height = 100,
   color = "var(--ru-primary-color)"
}) => {
  return (
    <Loading>
      <ThreeDots color={color} height={height} width={width} />
    </Loading>
  )
}

export default LoadingScreen