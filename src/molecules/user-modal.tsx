import React, { useState } from 'react'
import {
  Formik,
  FormikProps,
  Form,
  Field
} from 'formik'
import styled from 'styled-components'
import * as Yup from 'yup'

import { Modal } from '../organisms'
import {
  createUser,
  deleteUser,
  updateUser
} from '../common'
import { ButtonBase } from '../atoms'

const FormikFooter = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  justify-content: flex-end;
  gap: 1.5rem;
  margin-top: 1.5rem;
`

type UserProps = {
  modalAction: string,
  setDisplayModal: any,
  setLoading?: any,
  userDetails?: any,
  setDeletedUser?: any
}

interface UserValues {
  title: string,
  firstName: string,
  lastName: string,
  email: string
}

const userSchema = Yup.object().shape({
  title: Yup.string().required('Please select from the list'),
  firstName: Yup.string().required('Please enter your firstname'),
  lastName: Yup.string().required('Please enter your lastname'),
  email: Yup.string().email(`You've entered an invalid email address, please try again`).required('Please enter your email address'),
});

const UserModal: any = (props: UserProps) => {
  const createUserValues: UserValues = { title: '', firstName: '', lastName: '', email: '' }
  const updateUserValues: UserValues = { title: props.userDetails?.title, firstName: props.userDetails?.first_name, lastName: props.userDetails?.last_name, email: props.userDetails?.email }

  const deleteByUserId = async (userId: string) => {
    await deleteUser(userId)
    props.setLoading(false)
    props.setDeletedUser(true)
  }

  const createNewUser = async (user: any) => {
    await createUser(user)
    props.setDisplayModal(false)
    window.history.go(0)
  }

  const updatingUser = async (user: any, userId?: any) => {
    await updateUser(user, userId)
    props.setLoading(false)
    window.history.go(0)
  }

  const handleUserDelete = (action: string, user?: any, userId?: any) => {
    props.setDisplayModal(false)
    props.setLoading(true)

    deleteByUserId(userId)
  }

  const handleCancel = () => {
    props.setDisplayModal(false)
  }

  switch (props.modalAction) {
    case 'create':
      return (
        <Modal hideAction={true}
               heading={'Create a new user?'}
               modalWidth={500}
               actionTitle={'Create'}
               actionButtonType={'primary'}>
          <h2>You are about to create a new user</h2>
          <Formik
              initialValues={createUserValues}
              validationSchema={userSchema}
              onSubmit={(values, actions) => {
                createNewUser(values)
              }}
          >
            {(props: FormikProps<UserValues>) => {
              const {
                touched,
                errors,
              } = props
              return (
                <Form style={{ display: 'flex', flexDirection: 'column', gap: '1rem' }}>
                  <label htmlFor="title">Title</label>
                  <Field as="select" id="title" name="title" style={{ padding: '1rem', border: '1px solid var(--ru-border-tertiary-color)' }}>
                    <option value="">&lt; Select Below &gt;</option>
                    <option value="Mr.">Mr</option>
                    <option value="Mrs.">Mrs</option>
                    <option value="Miss.">Miss</option>
                    <option value="Dr.">Dr.</option>
                  </Field>
                  {errors.title && touched.title ? (
                      <div style={{ color: 'red', fontSize: '16px', fontWeight: '300' }}>{errors.title}</div>
                  ) : null}
                  <label htmlFor="firstName">First Name</label>
                  <Field id="firstName" name="firstName" placeholder="First Name" style={{ padding: '1rem', border: '1px solid var(--ru-border-tertiary-color)' }} />
                  {errors.firstName && touched.firstName ? (
                      <div style={{ color: 'red', fontSize: '16px', fontWeight: '300' }}>{errors.firstName}</div>
                  ) : null}
                  <label htmlFor="lastName">Last Name</label>
                  <Field id="lastName" name="lastName" placeholder="Last Name" style={{ padding: '1rem', border: '1px solid var(--ru-border-tertiary-color)' }} />
                  {errors.lastName && touched.lastName ? (
                      <div style={{ color: 'red', fontSize: '16px', fontWeight: '300' }}>{errors.lastName}</div>
                  ) : null}
                  <label htmlFor="email">Email</label>
                  <Field id="email" name="email" placeholder="Email" style={{ padding: '1rem', border: '1px solid var(--ru-border-tertiary-color)' }} />
                  {errors.email && touched.email ? (
                      <div style={{ color: 'red', fontSize: '16px', fontWeight: '300' }}>{errors.email}</div>
                  ) : null}
                  <FormikFooter>
                    <ButtonBase rounded buttonType="" onClick={handleCancel}>
                      Cancel
                    </ButtonBase>
                    <ButtonBase type={'submit'} buttonType={'primary'} rounded>Create</ButtonBase>
                  </FormikFooter>
                </Form>
              )
            }}
          </Formik>
        </Modal>
      )
      break
    case 'delete':
      return (
        <Modal hideAction={false}
               handleAction={() => handleUserDelete(props.modalAction, '', props.userDetails.id)}
               handleModal={() => props.setDisplayModal(false)}
               heading={'You are about to delete a user'}
               modalWidth={500}
               actionTitle={'Delete'}
               actionButtonType={'error'}>
          <p>This will delete {props.userDetails.first_name + " " + props.userDetails.last_name} from the results table? Are you sure?</p>
        </Modal>
      )
      break
    case 'edit':
      return (
        <Modal hideAction={true}
               heading={`Update ${props.userDetails.first_name + " " + props.userDetails.last_name} details`}
               actionTitle={'Update'}
               modalWidth={500}
               actionButtonType={'primary'}>
          <br />
          <br />
          <Formik
              initialValues={updateUserValues}
              validationSchema={userSchema}
              onSubmit={(values, actions) => {
                updatingUser(values, props.userDetails.id)
                props.setDisplayModal(false)
              }}
          >
            {(props: FormikProps<UserValues>) => {
              const {
                touched,
                errors,
              } = props
              return (
                  <Form style={{ display: 'flex', flexDirection: 'column', gap: '1rem' }}>
                    <label htmlFor="title">Title</label>
                    <Field as="select" id="title" name="title" style={{ padding: '1rem', border: '1px solid var(--ru-border-tertiary-color)' }}>
                      <option value="">&lt; Select Below &gt;</option>
                      <option value="Mr.">Mr</option>
                      <option value="Mrs.">Mrs</option>
                      <option value="Miss.">Miss</option>
                      <option value="Dr.">Dr.</option>
                    </Field>
                    {errors.title && touched.title ? (
                        <div style={{ color: 'red', fontSize: '16px', fontWeight: '300' }}>{errors.title}</div>
                    ) : null}
                    <label htmlFor="firstName">First Name</label>
                    <Field id="firstName" name="firstName" placeholder="First Name" style={{ padding: '1rem', border: '1px solid var(--ru-border-tertiary-color)' }} />
                    {errors.firstName && touched.firstName ? (
                        <div style={{ color: 'red', fontSize: '16px', fontWeight: '300' }}>{errors.firstName}</div>
                    ) : null}
                    <label htmlFor="lastName">Last Name</label>
                    <Field id="lastName" name="lastName" placeholder="Last Name" style={{ padding: '1rem', border: '1px solid var(--ru-border-tertiary-color)' }} />
                    {errors.lastName && touched.lastName ? (
                        <div style={{ color: 'red', fontSize: '16px', fontWeight: '300' }}>{errors.lastName}</div>
                    ) : null}
                    <label htmlFor="email">Email</label>
                    <Field id="email" name="email" placeholder="Email" style={{ padding: '1rem', border: '1px solid var(--ru-border-tertiary-color)' }} />
                    {errors.email && touched.email ? (
                        <div style={{ color: 'red', fontSize: '16px', fontWeight: '300' }}>{errors.email}</div>
                    ) : null}
                    <FormikFooter>
                      <ButtonBase rounded buttonType="" onClick={handleCancel}>
                        Cancel
                      </ButtonBase>
                      <ButtonBase type={'submit'} buttonType={'primary'} rounded>Update</ButtonBase>
                    </FormikFooter>
                  </Form>
              )
            }}
          </Formik>
        </Modal>
      )
      break
    default:
      break
  }
}

export default UserModal