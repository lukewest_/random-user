import React, { useContext } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChevronDown, faEye, faPencilAlt, faTrashAlt } from '@fortawesome/free-solid-svg-icons'
import styled from 'styled-components'

import {
    Accordion,
    AccordionContent,
    AccordionHeader,
    AccordionIcon,
    ButtonIcon,
    Table
} from '../atoms'
import { UserContext } from '../common'

const List = styled.ul`
  display: flex;
  align-items: center;
  justify-content: space-between;

  margin: 0 0 1.5rem;
  padding: 0;
  
  &:last-of-type {
    margin-bottom: 0;
  }
  
  > li {
    list-style-type: none;
  }
`

const ListHeaderItem = styled.li`
  color: var(--ru-border-primary-color);
  font-size: 14px;
  font-weight: 400;
`

const ListItem = styled.li`
  display: inline-flex;
  align-items: center;
  justify-content: space-between;
  gap: .5rem;
  
  color: var(--ru-heading-color);
  font-size: 14px;
`

type UserResultProps = {
    handleUser: any
}

export const UserResult: React.FC<UserResultProps> = (props) => {
    const { users } = useContext(UserContext)

    return (
        <>
            {
                Array.isArray(users) && (
                    users.map((item: any, index: number) => {
                        return (
                            <Accordion key={index}>
                                <AccordionHeader>
                                    <AccordionIcon className="accordion__header-icon">
                                        <FontAwesomeIcon icon={faChevronDown}/>
                                    </AccordionIcon>
                                    { item.first_name } { item.last_name }
                                </AccordionHeader>
                                <AccordionContent>
                                    <List>
                                        <ListHeaderItem>Email</ListHeaderItem>
                                        <ListItem>{ item.email }</ListItem>
                                    </List>
                                    <List>
                                        <ListHeaderItem>Account Status</ListHeaderItem>
                                        <ListItem>{ item.status ? 'Active' : 'Disabled' }</ListItem>
                                    </List>
                                    <List>
                                        <ListHeaderItem>Action</ListHeaderItem>
                                        <ListItem>
                                            <ButtonIcon buttonType="primary" rounded onClick={(e) => props.handleUser(e, item.id)}>
                                                <FontAwesomeIcon icon={faEye}/>
                                            </ButtonIcon>
                                        </ListItem>
                                    </List>
                                </AccordionContent>
                            </Accordion>
                        )
                    }
                ))
            }
            <Table>
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Account Status</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    {
                        Array.isArray(users) && (
                            users.map((item: any, index: number) => {
                                return (
                                    <tr key={index}>
                                        <td>{ item.id }</td>
                                        <td>{ item.first_name } { item.last_name }</td>
                                        <td>{ item.email }</td>
                                        <td>{ item.status ? 'Active' : 'Disabled' }</td>
                                        <td>
                                            <ButtonIcon buttonType="primary" rounded onClick={(e) => props.handleUser(e, item.id)}>
                                                <FontAwesomeIcon icon={faEye}/>
                                            </ButtonIcon>
                                        </td>
                                    </tr>
                                )
                            }
                        ))
                    }
                </tbody>
            </Table>
        </>
    )
}

export default UserResult