import { createGlobalStyle } from 'styled-components'

export const GlobalStyle = createGlobalStyle`
  html {
    --ru-theme-color: #333333;
    
    --ru-primary-color: #582c83;
    --ru-secondary-color: #f8f8f8;
    --ru-tertiary-color: #282c34;
    
    --ru-white-color: #ffffff;
    --ru-black-color: #333333;

    --ru-border-primary-color: #4c5463;
    --ru-border-secondary-color: #717c92;
    --ru-border-tertiary-color: #ececec;
    
    --ru-heading-color: #a0a8b6;
    --ru-text-color: #d0d3db;

    --ru-success-color: #198754;
    --ru-info-color: #0d6efd;
    --ru-error-color: #bb2d3b;
  }

  body {
    margin: 0;
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen',
    'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue',
    sans-serif;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }

  code {
    font-family: source-code-pro, Menlo, Monaco, Consolas, 'Courier New',
    monospace;
    color: var(--ru-primary-color);
  }
  
  h1, h2, h3, h4, h5, h6 {
    color: var(--ru-primary-color);
    font-weight: bold;
    margin: 0 0 1.5rem 0;
    padding: 0;
    line-height: 1;
  }
  
  p,
  span {
    font-size: 17px;
    color: var(--ru-text-color);
    margin: 0 0 1.5rem 0;
  }
  
  a,
  p > a {
    color: var(--ru-primary-color);
    text-decoration: none;
    transition: color 0.2s ease-out 0s;
    cursor: pointer;
    
    &:hover,
    &:focus {
      color: var(--ru-primary-color);
      border-bottom: 1px solid var(--ru-primary-color);
      text-decoration: none;
    }
  }
`