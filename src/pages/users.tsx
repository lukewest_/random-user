import React, { useContext, useEffect, useState } from 'react'

import {
    ButtonBase,
    MainTitle,
    Section
} from '../atoms'
import {
    LoadingScreen,
    UserResults,
    UserModal
} from '../molecules'

type UsersProps = {
    loading: any
}

export const Users: React.FC<UsersProps> = (props) => {
    const [displayModal, setDisplayModal] = useState(false)
    const [modalAction, setModalAction] = useState('')
    const [createdUser, setCreatedUser] = useState(false)

    const handleUserModal = (e: any, action: string) => {
        e.stopPropagation()

        if (e.key && e.key.toLowerCase() === 'escape') {
            setDisplayModal(false)
        } else if (e.key) return

        setModalAction(action)
        setDisplayModal(true)
    }

    const handleUser = (e: any, userId: string) => {
        e.preventDefault()
        window.location.href = `/${userId}`
    }

    return (
        <>
            <MainTitle><h1>Users</h1></MainTitle>
            <Section>
                <ButtonBase buttonType={'primary'} rounded onClick={(e) => handleUserModal(e, 'create')}>
                    Create User
                </ButtonBase>
            </Section>
            <Section className="section__results">
                <UserResults handleUser={handleUser} />
                { props.loading && <LoadingScreen/> }
            </Section>
            {
                displayModal && <UserModal modalAction={modalAction}
                                           setLoading={props.loading}
                                           setDisplayModal={setDisplayModal} />
            }
        </>
    )
}

export default Users