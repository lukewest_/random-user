import React, { useEffect, useState } from 'react'
import { Link, useParams } from 'react-router-dom'

import {
    Accordion,
    AccordionContent,
    AccordionHeader,
    AccordionIcon,
    Alert,
    ButtonIcon,
    MainTitle,
    Section
} from '../atoms'
import {
    LoadingScreen,
    UserModal
} from '../molecules'
import {
    getUserById
} from '../common'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronLeft, faChevronDown, faPencilAlt, faTrashAlt } from "@fortawesome/free-solid-svg-icons";
import styled from "styled-components";

const List = styled.ul`
  display: flex;
  align-items: center;
  justify-content: space-between;

  margin: 0 0 1.5rem;
  padding: 0;
  
  &:last-of-type {
    margin-bottom: 0;
  }
  
  > li {
    list-style-type: none;
  }
`

const ListHeaderItem = styled.li`
  color: var(--ru-border-primary-color);
  font-size: 14px;
  font-weight: 400;
`

const ListItem = styled.li`
  display: inline-flex;
  align-items: center;
  justify-content: space-between;
  gap: .5rem;
  
  color: var(--ru-heading-color);
  font-size: 14px;
`

export const User = () => {
    const { id } = useParams()
    const [displayModal, setDisplayModal] = useState(false)
    const [deletedUser, setDeletedUser] = useState(false)
    const [loading, setLoading] = useState(true)
    const [modalAction, setModalAction] = useState('')
    const [userDetails, setUserDetails] = useState({})
    const [user, setUser] = useState<any | undefined>('');

    const loadUserById = async (id: any) => {
        const response = await getUserById(id)
        setLoading(false)
        setUser(response)
    }

    const handleUser = (e: any, details: string, action: string) => {
        e.stopPropagation()

        if (e.key && e.key.toLowerCase() === 'escape') {
            setUserDetails({})
        } else if (e.key) return

        setModalAction(action)
        setUserDetails(details)
        setDisplayModal(true)
    }

    const redirectToUsers = () => {
        window.history.go(-1)
    }

    useEffect(() => {
        loadUserById(id)
        console.log("deleteduser... " + deletedUser)
    }, [])

    return (
        <>
            <Section>
                <a role={'button'} onClick={redirectToUsers}>
                    <FontAwesomeIcon icon={ faChevronLeft } />&nbsp;Back to Users List
                </a>
            </Section>
            {
                deletedUser ? (
                    <Alert buttonType={'success'}>This user has been deleted, please click back to the list of users by the link above.</Alert>
                ) : (
                    loading ? (
                        <LoadingScreen/>
                    ) : (
                        <Accordion>
                            <AccordionHeader>
                                <AccordionIcon className="accordion__header-icon">
                                    <FontAwesomeIcon icon={faChevronDown}/>
                                </AccordionIcon>
                                { user.first_name } { user.last_name }
                            </AccordionHeader>
                            <AccordionContent>
                                <List>
                                    <ListHeaderItem>Email</ListHeaderItem>
                                    <ListItem>{ user.email }</ListItem>
                                </List>
                                <List>
                                    <ListHeaderItem>Account Status</ListHeaderItem>
                                    <ListItem>{ user.status ? 'Active' : 'Disabled' }</ListItem>
                                </List>
                                <List>
                                    <ListHeaderItem>Action</ListHeaderItem>
                                    <ButtonIcon buttonType="error" rounded onClick={(e) => {handleUser(e, user, 'delete')}}>
                                        <FontAwesomeIcon icon={ faTrashAlt } />
                                    </ButtonIcon>
                                    <ButtonIcon buttonType="primary" rounded onClick={(e) => {handleUser(e, user, 'edit')}}>
                                        <FontAwesomeIcon icon={ faPencilAlt } />
                                    </ButtonIcon>
                                </List>
                            </AccordionContent>
                        </Accordion>
                    )
                )
            }
            {
                displayModal && <UserModal modalAction={modalAction}
                                           setDisplayModal={setDisplayModal}
                                           setLoading={setLoading}
                                           userDetails={userDetails}
                                           setDeletedUser={setDeletedUser} />
            }
        </>
    )
}

export default User