import React, { useEffect, useState, useRef } from 'react'

import {
    Container
} from '../atoms'
import {
    Main
} from '../organisms'

const Layout = () => {
    return (
        <Container>
            <Main />
        </Container>
    )
}

export default Layout